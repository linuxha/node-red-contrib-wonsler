// Okay wonsler isn't a great name (once-ler)
// basically the idea behind this is that a sends something and the wonsler-out
// stows the value in <user_var_name>Last. Then the wonsler-in checks to see
// if it's current value matches the last value. If it does it drops the
// message. If it doesn't it lets the msg pass. Either way the
// <user_var_name>Last is cleared.
//
module.exports = function(RED) {
    "use strict";

    this.log("Wonsler starting");

    function wonslerIn(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
	    var tag = 'userTag';
	    // If the output node saved the value to to xxx
	    // and it matches the current value, we drop it
	    // otherwise we let it pass
            lastMsg = global.get('lastMsg' + tag) || '';
	    if(lastMsg == msg.payload) {
		global.set('lastMsg' + tag, ''); // Would undefined be better?
	    } else {
		global.set('lastMsg' + tag, ''); // Would undefined be better?
		node.send(msg);
	    }
        });
    };

    RED.nodes.registerType("wonsler-In", wonslerIn);

    function wonslerOut(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
	    var tag = 'userTag';
	    // If the output node saved the value to to xxx
	    // and it matches the current value, we drop it
	    // otherwise we let it pass
            //msg.payload = msg.payload.toLowerCase();
	    global.set('lastMsg' + tag, msg.payload);
            node.send(msg);
        });
    };

    RED.nodes.registerType("wonsler-Out", wonslerOut);

    this.log("Wonsler started");
}

