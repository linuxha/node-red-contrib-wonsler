# Wonsler README

# Description
The wonsler nodes should stop a node from repeating the same message twice. There are a couple of different nodes, the wonsler and the wonsler-In and wonsler-Out pair.

This should be useful when a node, such as mytimeout has it's input and output connected to an MQTT node with the same topic. The wonlser nodes will stop the same message from being sent stopping any feedback loop that might occur.

At the moment this is mostly me experimenting with creating new nodes.

# Author
Neil Cherry <ncherry@linuxha.com>

# Copyright
2017

# License
ISC

